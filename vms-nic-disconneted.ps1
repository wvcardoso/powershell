# Select all vms with nic disconneted

Connect-VIServer -Server vcenter -Protocol https -User willian

#$vms=get-content c:\Users\willian\Downloads\vms.txt
$vms="vm1 vm2 vm3"
foreach ($vm in $vms){
    get-vm -Name $vm | Get-NetworkAdapter | Select-Object @{N="VM";E={$_.Parent.Name}},Name,ConnectionState | Where { $_.ConnectionState.Connected -eq $false }
}